/*
 * File:   main.c
 * Author: Jesu
 *
 * Created on May 22, 2018, 1:50 PM
 */

#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low Voltage In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

#define _XTAL_FREQ     20000000

#include "TK-02-05-01_EPSController.h"

unsigned char temp;
char buff[6];
unsigned int value;

void main(void) {

    ADCON1 = ANALOGUE_PIN_SETUP;    
    GPIOInit();
    spiMasterInit();
    EpsSPImasterInit();

	GIE = 0;

    uartInit(UART_BAUD); // Initialize UART @ 9600 baud.
   
    spiSlaveInit();
    __delay_ms(500);
    
    i2cSlaveInit(&picMAddr);   //the address for the slave is set to 0x30 (b00110000). This depends on the user and will be the address of the device in slave mode to be able to hear and establish i2c data transmission
    __delay_ms(500);
   
    TIMER2Init(); //TIMER 2
    
    //This is maybe included in the functions
    //INTEDG = 0; // Set RB0 interrupt on falling edge.
    //INTF = 0;   // Initialize RB0/INT's flag bit.
    //INTE = 1;   // Enable interrupt by RB0.
    //PEIE = 1;   // Enable peripheral interrupts
    //GIE = 1;  

    //Init_StateMachine();  
    //TESTING FUNCTIONS:
    test_Enable();
    while(1){
        TIMERCount(multiplier); // This one receives a multiplier and 
    	//StateMachine(); 
        //TESTING FUNCTIONS:
        test_read_ADC(ADCdata);
    }  
}

void interrupt globalInterrupt()
{
    i2cInterrupt(command, answer);
     
//    if(INTF) // RB0/INT External Interrupt took place.
//    {
//        SPI_FLAG = 1;
//        temp=spiSlaveReadBytesPIC(command);
//        temp=spiSlaveWriteBytesPIC(answer,sizeof(answer));
//        INTF = 0; // Clear INTF
//    }    
}
