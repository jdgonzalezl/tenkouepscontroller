# TenkouEPSController

This is a repository for software development of *TK-02-05-01 EPSController* board for Ten-Koh mission. It contains two projects, one for each
PIC located within the EPS Controller PCB as well as the code that is shared between them.

# Folder structure

## EPS Controller

This repository contains projects for two PICs, *PIC_M* and *PIC_B*. Each of them is located in a different folder to avoid having to switch
repository branches or use compiler pragmas to select code for each PIC (each PIC needs a different `main` function, and only one main is
allowed in a project). The code shared between the two PICs, including the `TK-02-05-01_EPSController.h` file that contains the pin definition
of the two PICs, is located in a separate folder called *shared*.

## Libraries

The PIC projects in this repository use several Tenkou software libraries that are shared with other subsystems. To make sure that these
libraries are found where MPLAB expects them (and to avoid having to change the project `configuration.xml` all the time), please clone the
shared libraries into folders with the following names (defaults used by git):

* tenkouadc,
* tenkouepscontroller,
* tenkougpio,
* tenkoui2cslave,
* TenkouSDCard,
* tenkouspimaster,
* tenkouspislave,
* tenkouuart.

For a list of the owners of these repositories, please check this [Google Drive Sheet](https://docs.google.com/spreadsheets/d/1mfUhjpLLei_G4BwL3_K3ghEvJ2L69L-y8f4Ou1xm3Q0/edit?usp=sharing).