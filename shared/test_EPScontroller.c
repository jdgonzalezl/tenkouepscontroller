/*
 * File:   test_EPScontroller.c
 * Author: Jesu
 *
 * Created on May 31, 2018, 6:42 PM
 */
#include "TK-02-05-01_EPSController.h"
#include "test_EPScontroller.h"

void test_read_ADC(uint16_t ADCdata[]){
    float volt;
    char* buff;
    int* status;
    char i; 
    MCU_BCU_RESET_INIT=0;
    MCU_BCU_RESET =0;
    
    uartWriteBytes("OCP:",4);
    read_ADC(&PORTD,5,ADCdata); //RD5 for OCP ADC
         for(i=0; i <8; i++){
            volt = adc_conversion_voltage(ADCdata[i],0);
            buff = ftoa(volt, &status);
            uartWriteBytes(buff,8);  
         } 
    __delay_ms(500);
    uartWriteBytes("\nIFSW:",6);
    read_ADC(&PORTD,4,ADCdata); //RD4 for IFSW ADC
         for(i=0; i <8; i++){
            volt = adc_conversion_voltage(ADCdata[i],0);
            buff = ftoa(volt, &status);
            uartWriteBytes(buff,8);  
         } 
    __delay_ms(500);
    uartWriteBytes("\nBUS:",5);
    read_ADC(&PORTB,6,ADCdata); //RB6 for RDS_BUS ADC
         for(i=0; i <8; i++){
            volt = adc_conversion_voltage(ADCdata[i],0);
            buff = ftoa(volt, &status);
            uartWriteBytes(buff,8);  
         } 
    __delay_ms(500);
    uartWriteBytes("\nPL:",4);
    read_ADC(&PORTB,7,ADCdata); //RB7 for RDS_PL ADC
         for(i=0; i <8; i++){
            volt = adc_conversion_voltage(ADCdata[i],0);
            buff = ftoa(volt, &status);
            uartWriteBytes(buff,8);  
         } 
    uartWriteBytes("\n\r",2);
    __delay_ms(500);
}
 
void test_Enable(void){
    //Turn Bus Power lines
    Enable(COM1_SW);
    Disable(COM2_SW);
    Enable(OBC_SW);
    Disable(UCP_SW);
    Enable(PL_SW);
    Enable(DLP_SW);
    Disable(CPD_SW);
    Disable(ADS_SW);
    
}