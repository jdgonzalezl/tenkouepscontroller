/* Ten-Koh TK-02-05-01 EPS COntroller.
 * 
 * Intended for use with PIC16F877 and version of the PCB at least 1.1.6.
 */
#include "TK-02-05-01_EPSController.h"

void EPS_adc_read(void)    
{            
    uint16_t data = 0; // Raw conversion results from the ADC.
    unsigned char j = 0;
    unsigned char i = 0;
    // Defines the CS for the ADC and its measurement settings.
    adc_communication_information_t trans_data;
    while (j < 3){
        if (j == 0){
            trans_data.port = &PORTB;
            trans_data.pin = 6;
        } else if (j == 1){
            trans_data.port = &PORTB;
            trans_data.pin = 7;
        } else if (j == 2){
            trans_data.port = &PORTD;
            trans_data.pin = 4;
        }

        trans_data.range_select = DOUBLE_RANGE;
        trans_data.outcoding_select = STRAIGHT_BINARY;
                
        for( i=0; i<8; i++){
            trans_data.channel_select = i;
            data = adc_communication_normal(trans_data);
            //data = 10*j + i;
            data_1[j*8 + 2*i] = (data & 0x0F00) >> 8;
            data_1[j*8 + 2*i + 1] = (data & 0xFF);
        }
        j++;
    }
}

void GPIOInit(void){
    FIVE_V_COM1_EN_INIT=0; FIVE_V_COM2_EN_INIT=0; FIVE_V_OBC_EN_INIT=0;
    FIVE_V_UCP_EN_INIT=0; FIVE_V_PL_EN_INIT=0; FIVE_V_DLP_EN_INIT=0;
    TWELVE_V_CPD_EN_INIT=0; TWELVE_V_ADS_EN_INIT=0;
    HEAT_EN_INIT=0; HEAT_ON_2_INIT=0; HEAT_ON_1_INIT=0;
    KILL_SET_INIT=0; KILL_RESET_INIT=0; PIC_SENSE_RESET_INIT=0; 
    GPIO_OBC_EPS_INIT=0;
    
    
}

void EpsSPImasterInit(void){
/*Initialize CS pins as output and Set pins
 */    
    SPI_EPS_CS_OCPSTMUX_INIT = 0; //
    SPI_EPS_CS_BATT_ADC_INIT = 0; // Analogue pin, so set ADCON.  
    SPI_EPS_CS_REGBUS_ADC_INIT = 0;
    SPI_EPS_CS_REGPL_ADC_INIT = 0;
    SPI_EPS_CS_SD_INIT=0;
    
    SPI_EPS_CS_OCPSTMUX=1;
    SPI_EPS_CS_SD=1; // REG_PL ADC.
    SPI_EPS_CS_REGPL_ADC=1; // REG_PL ADC.
    SPI_EPS_CS_REGBUS_ADC=1; // REG_BUS ADC.
    SPI_EPS_CS_BATT_ADC=1; // ADC in IF_SW that measures the battery temperatures, voltages and currents.
}
/**************************************************************************************
 * SetShortAnswer  (MAYBE IT WILL DISSAPEAR)
 *
 * Receive the raw answer to be send usign I2C, shift it one place to the right and add
 * the size in byte 0 and the 0xFF in byte "size"
 *
 * Attributes
 * -----------
 *  * fill: array of bytes that contains the raw answer to be send, without size or 0xFF.
 *  * size: no. of bytes that will be sent from fill, a 0xFF will be put in that position. 
 **************************************************************************************/

 void SetShortAnswer(unsigned char* fill, unsigned char size ){
     uint8_t temp = 0;
     int8_t i = 0;
     CLEAR_ANSWER();
     for (i = size + 1; i > 0; i--){
 		answer[i] = fill[i - 1];	
 	}
     answer[size + 1] = 0xFF;
     answer[0] = size + 1;
     CLEAR_COMMAND(); //If we decided to send answer is because we don't need anything else from the command
 }

/**************************************************************************************
 * SetAnswer 
 *
 * Receive the beacon and status parts of the answer shift it one place to the right and add
 * the size in byte 0 and the 0xFF in byte "size"
 *
 * Attributes
 * -----------
 *  * hk: array of bytes that contains the ASCII data of HK.
 *  * hk_size: no. of bytes that will be sent from hk.
 *  * stat: array of bytes that contains the HEX data of status.
 *  * stat_size: no. of bytes that will be sent from stat. 
 **************************************************************************************/

void SetAnswer(unsigned char* hk, unsigned char hk_size, unsigned char* stat, unsigned char stat_size){
    int8_t i = 0;
    unsigned char size; 
    CLEAR_ANSWER();
    size = hk_size + stat_size + 1;
    for (i = 0; i <= size; i++){
   		if (i == 0){
            answer[i] = size;
        }else if (i > hk_size){
   			answer[i] = stat[i - hk_size -1];	
   		}else{
   			answer[i] = hk[i - 1];
   		}		
	}
    answer[size] = 0xFF;
}


/**************************************************************************************
 * PrepareStatus
 *
 CHANGE
 * Receive an array of hex values, each two measurements should represent a 12 bytes ADC
 * measurement. This function will create an ASCII value between 00 and 99 to represent
 * the 0 to 5 volt range of the ADC measurement. 
 * 
 * Attributes
 * -----------
 *  * data: array of bytes that contains two bytes per 12 bit ADC.
 *  * size: no. of bytes that will be extracted from data and converted, each 2 bytes
 *	represent an 12 bits ADC measurent. 
 **************************************************************************************/

void PrepareStatus(void){
	unsigned char st = 0;
	status_reg[0] = (unsigned char) multiplier;
    st = (unsigned char)((FIVE_V_COM1_EN << 7) ^ 0x80);       
	st += (unsigned char)((FIVE_V_COM2_EN << 6) ^ 0x40); 
	st += (unsigned char)((FIVE_V_UCP_EN << 5) & 0x20);
	st += (unsigned char)((FIVE_V_PL_EN << 4) & 0x10);
	st += (unsigned char)((FIVE_V_DLP_EN << 3) & 0x08);
	st += (unsigned char)((TWELVE_V_CPD_EN << 2) & 0x04);
	st += (unsigned char)((TWELVE_V_ADS_EN << 1) & 0x02);
	st += (unsigned char)((HEAT_EN) & 0x01);	
	status_reg[1] = st;
    st = 0;
    st = (unsigned char)((PIC_SENSE_RESET << 7) & 0x80);       
	st += (unsigned char)((SD_EN << 6) & 0x40); 
	st += (unsigned char)((FIVE_V_OBC_EN << 5) ^ 0x20);	
    status_reg[2] = st; 
}



/**************************************************************************************
 * PrepareHK
 *
 * Receive an array of uint16 where each element represent a 12 bytes ADC
 * measurement. This function will create an ASCII value between 00 and 99 to represent
 * the 0 to 5 volt range of the ADC measurement. 
 * 
 * Attributes
 * -----------
 *  * data: array of int that contains two bytes per 12 bit ADC.
 *  * size: no. of bytes that will be extracted from data and converted, each 2 bytes
 *	represent an 12 bits ADC measurent. 
 **************************************************************************************/

void PrepareHK(uint16_t* data, unsigned char size){
    uint16_t value;
    int8_t i = 0;    
    char buff[8];
    for (i = 0; i < size; i++){
        value = (uint16_t) (data[i] * 0.02441);
        if (value > 99) value = 99;
        utoa(buff, value, 10);
        if (value < 0x10){
            buff[1] = buff[0];
            buff[0] = '0';
        }
        hk_string[i] = buff[0];
        hk_string[i + 1] = buff[1];
        memset(buff,0,sizeof(buff));        
    }        
}

/**************************************************************************************
 * TIMER2Init
 *
 * Configure the timer 2 module usign a 1:16 postscaler and a 16 prescaler and
 * a period register of 250 instruction cycles to get a total of 12.8 ms cycles.
 * 
 **************************************************************************************/

void TIMER2Init(void){
	//TOUTPS3:TOUTPS0 = 1111,  1:16 Postscaler
	TOUTPS3 = 1;
	TOUTPS2 = 1;
	TOUTPS1 = 1;
	TOUTPS0 = 1;
	TMR2ON = 1; //Timer 2 ON
	//T2CKPS1:T2CKPS0 = 11,	16 Prescaler
	T2CKPS1 = 1;
	T2CKPS0 = 1;
	PR2 = 0b11111010; //250 
	//This leads to 12.8 ms
}

/**************************************************************************************
 * TIMERCount
 *
 * Set the SEC_FLAG byte every "fmult * 0.9984s". This value comes form 78 * 12.8ms = 998.4ms
 * Check TIMER2Init for additional information. 
 *
 * Attributes
 * -----------
 *  * fmult: scaler to set the number of seconds of the counter. 
 **************************************************************************************/

void TIMERCount(uint8_t fmult){
	if (TMR2IF){
		count1 --;
        count2 --;
		TMR2IF = 0;
	}
	if (count1 <= 0){
		count1 = (int16_t) (TIM_CYC * fmult);
		SEC_FLAG = 1;
	} 
    if (count2 <= 0){
		count2 = (int16_t) (H_TIM_CYC * fmult);
		H_SEC_FLAG = 1;
	} 
}

/**************************************************************************************
 * Enable
 *
 * Check the received subcommand and turn on a load usign that information
 *
 * Attributes
 * -----------
 *  * command: char that contains the sub-command to define which load to turn on. 
 **************************************************************************************/

void Enable(unsigned char command){
	switch(command){
        case COM1_SW:
            FIVE_V_COM1_EN = 0;
            break;
        case COM2_SW:
            FIVE_V_COM2_EN = 0;
            break;
        case UCP_SW:
            FIVE_V_UCP_EN = 1;
            break;
        case PL_SW: 
            FIVE_V_PL_EN = 1;
            break;
        case DLP_SW:
            FIVE_V_DLP_EN = 1;
            break;
        case CPD_SW:
            TWELVE_V_CPD_EN = 1;
            break;
        case ADS_SW:
            TWELVE_V_ADS_EN = 1;
            break;
        case HEAT_SW:
            HEAT_EN = 1;
            break;
        case PIC_SENSE:
            PIC_SENSE_RESET = 1;
            break;
        case SD_CARD:
            SD_EN = 1; //I'm not sure if this logic is correct
            break;
        case OBC_SW:
            FIVE_V_OBC_EN = 0;
            break;
  	}
}

/**************************************************************************************
 * Disable
 *
 * Check the received subcommand and turn off a load usign that information
 *
 * Attributes
 * -----------
 *  * command: char that contains the sub-command to define which load to turn off. 
 **************************************************************************************/

void Disable(unsigned char command){
	switch(command){
        case COM1_SW:
            FIVE_V_COM1_EN = 1;
            break;
        case COM2_SW:
            FIVE_V_COM2_EN = 1;
            break;
        case UCP_SW:
            FIVE_V_UCP_EN = 0;
            break;
        case PL_SW: 
            FIVE_V_PL_EN = 0;
            break;
        case DLP_SW:
            FIVE_V_DLP_EN = 0;
            break;
        case CPD_SW:
            TWELVE_V_CPD_EN = 0;
            break;
        case ADS_SW:
            TWELVE_V_ADS_EN = 0;
            break;
        case HEAT_SW:
            HEAT_EN = 0;
            break;
        case PIC_SENSE:
            PIC_SENSE_RESET = 0;
            break;
        case SD_CARD:
            SD_EN = 0; //I'm not sure if this logic is correct
            break;
        case OBC_SW:
            FIVE_V_OBC_EN = 1;
            break;
	}
}

/**************************************************************************************
 * Reset
 *
 * Check the received subcommand and reset a load usign that information
 *
 * Attributes
 * -----------
 *  * command: char that contains the sub-command to define which load to reset. 
 **************************************************************************************/

void Reset(unsigned char command){
	switch(command){
        case COM1_SW:
            FIVE_V_COM1_EN = 1;
            __delay_ms(100);
            FIVE_V_COM1_EN = 0;
            break;
        case COM2_SW:
            FIVE_V_COM2_EN = 1;
            __delay_ms(100);
            FIVE_V_COM2_EN = 0;
            break;
        case UCP_SW:
            FIVE_V_UCP_EN = 0;
            __delay_ms(100);
            FIVE_V_UCP_EN = 1;
            break;
        case PL_SW: 
            FIVE_V_PL_EN = 0;
            __delay_ms(100);
            FIVE_V_PL_EN = 1;
            break;
        case DLP_SW:
            FIVE_V_DLP_EN = 0;
            __delay_ms(100);
            FIVE_V_DLP_EN = 1;
            break;
        case CPD_SW:
            TWELVE_V_CPD_EN = 0;
            __delay_ms(100);
            TWELVE_V_CPD_EN = 1;
            break;
        case ADS_SW:
            TWELVE_V_ADS_EN = 0;
            __delay_ms(100);
            TWELVE_V_ADS_EN = 1;
            break;
        case HEAT_SW:
            HEAT_EN = 0;
            __delay_ms(100);
            HEAT_EN = 1;
            break;
        case PIC_SENSE:
            PIC_SENSE_RESET = 0;
            __delay_ms(100);
            PIC_SENSE_RESET = 1;
            break;
        case SD_CARD:
            SD_EN = 0; //Doesn't make sense, better just do nothing
            __delay_ms(100);
            SD_EN = 1;
            break;
	}
}

void interruptInit(void){
    INTEDG = 0; // Set RB0 interrupt on falling edge.
    INTF = 0;   // Initialize RB0/INT's flag bit.
    INTE = 1;   // Enable interrupt by RB0.
    PEIE = 1;   // Enable peripheral interrupts
    GIE = 1;  
}


void read_ADC(volatile unsigned char *port, unsigned char pin, uint16_t data[]){
/*  Read 8 channels of ADC returning the conversion in the range of 0 to 4095
 *  Precondition:
 *  SPI master pin must be initialized
 *  CS of slave also must be initialized as output and set to high
 * 
 *  Parameters:
 *  *port   -   define the PORT where CS is set up: PORTD, PORTB
 *  pin     -   number of PORT
 *  data[]  -   array where conversion is stored 
 * 
 *  Example:
 *  read_ADC(&PORTD,5,ADCdata); //Read ADC with CS in RD5 (OCP) and stoe in ADCdata     
*/
    adc_trans_data.port = port;
    adc_trans_data.pin = pin; // RD4 for IF_SW.
    adc_trans_data.range_select = DOUBLE_RANGE;
    adc_trans_data.outcoding_select = STRAIGHT_BINARY;
    for(unsigned char i=0; i<8; i++){
        adc_trans_data.channel_select = i;
        data[i] = adc_communication_normal(adc_trans_data);
    }
    
}
