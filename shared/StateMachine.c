/* Ten-Koh TK-02-05-01 EPS COntroller.
 * 
 * Intended for use with PIC16F877 and version of the PCB at least 1.1.6.
 */
#include "TK-02-05-01_EPSController.h"
#include "StateMachine.h"

unsigned char cmd;
unsigned char scmd;

void Init_StateMachine (){
	ALL_LOAD_OFF();
	state = INIT; // State Machine always starts in INIT state
    //more initialization stuff here
}

void StateMachine (){
	switch (state){
        case INIT: //this state initialize the EPS Controller
            KILL_SET = 1; // Set the relay
            __delay_ms(100); // wait
            KILL_SET = 0; // Deenergize the coil
            FIVE_V_COM1_EN = 0;     //Turn ON COM1
            FIVE_V_COM2_EN = 0;     //Turn ON COM2  
            FIVE_V_OBC_EN = 0;  //Turn ON OBC
            // TURN OFF ALL OTHER POWER LINES (but we know they are off)
            FIVE_V_UCP_EN = 0; // All PL lines are active high, so off right now. 
            FIVE_V_PL_EN = 0; 
            FIVE_V_DLP_EN = 0;
            TWELVE_V_CPD_EN = 0;
            TWELVE_V_ADS_EN = 0;
            PREPANDSET();
            __delay_ms(100);
            state = DEFAULT; 
            break;
        
        case DEFAULT:             
            //Read ADC of IF_SW
            // | BAT_1_I | BAT_1_V | BAT_1_T | BAT_2_T | BAT_I_2 | BAT_2_V
            read_ADC(&PORTD,4,ADCdata); 
            ////HEATER ON/OFF goes here
            // TO DO: OBC ALERT GOES HERE
            PrepareHK(ADCdata, 6);
            PrepareStatus();
            SetAnswer(hk_string,10,status_reg,3);  
            //DETECT IF THERE IS A COMMAND
            if(command[command[0]] != 0){  //command[1] != 0
                state = CHKCMD;
            }               
            break;

        case CHKCMD: //This state interprets the commands send by the OBC and decide what to do
            switch (command[1]){
                case C_OBC_TICK:
                    if (TWELVE_V_ADS_EN == 0){
                        Enable(ADS_SW); 
                    }else{
                        Disable(ADS_SW); 
                    }
                    break;                
                case C_SETON:
                    Enable(command[2]);
                    break;      
                case C_SETOFF:
                    Disable(command[2]);
                    break;
                case C_SETRST:
                    Reset(command[2]);
                    break;
                case C_SET_MODE:
                    mode = (uint8_t) command[2];            // The mode is defined using complementary command (0x00, 0x01, etc)
                    break;
                case C_SENDDH:
                    state = SENDDH;
                    break;
                case C_KILL:
                    KILL_RESET = 1; // Reset the relay
                    __delay_ms(100); // wait
                    KILL_RESET = 0; // Deenergize the coil
                    break;
                case C_NO_KILL:
                    KILL_SET = 1; // Set the relay
                    __delay_ms(100); // wait
                    KILL_SET = 0; // Deenergize the coil
                    break;
                case C_TM_FREQ:
                    multiplier = (uint8_t) command[2];     // The multiplier is defined using complementary command (0x00, 0x01, etc)
                    break;
                case D_SATRST:
                    ALL_LOAD_OFF(); 
                    state = INIT;
                    break;
                case D_GPIO_OBC:
                    //SEND INTERRUPTION TO OBC
                    break;
            }
            CLEAR_COMMAND();
            //Prepare HK and Status data is moved to DEFAULT after reading ADCs???                
            state = DEFAULT;
            break; 

        case OBCRESET:
            FIVE_V_OBC_EN = 0;  //turn OFF OBC
            __delay_ms(100);    // wait a little before getting inside next state
            state = INIT;  
            break;

        case SENDDH:
            //do the session
            state = DEFAULT; //come back to INIT
            break;
    }
}
