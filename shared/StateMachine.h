/* Ten-Koh EPS State Machine header file, defines the ENUM and variables for
 * the State Machine of the EPS controller. 
 * Intended for use with PIC16F877
 */
#ifndef STATE_MACHINE
#define	STATE_MACHINE


enum states { INIT = 0, DEFAULT = 1,  CHKCMD = 2, OBCRESET = 3, LOADSW = 4, SENDDH = 5, KILL = 6, SENDHK = 7 };   //this define all the possible states of the machine

#define COMMAND_SIZE	20
#define ANSWER_SIZE		20


#define CLEAR_COMMAND() {memset(command,0,COMMAND_SIZE);}
#define CLEAR_ANSWER() 	{memset(answer,0,ANSWER_SIZE);}
#define ALL_LOAD_OFF()	{FIVE_V_OBC_EN = 1; FIVE_V_COM1_EN = 1; FIVE_V_COM2_EN = 1; FIVE_V_UCP_EN = 0; FIVE_V_PL_EN = 0; FIVE_V_DLP_EN = 0; TWELVE_V_CPD_EN = 0; TWELVE_V_ADS_EN = 0;}
#define PREPANDSET()    {PrepareHK(hk_data, 10); PrepareStatus(); SetAnswer(hk_string,10,status_reg,3); CLEAR_COMMAND();}

unsigned char state;
unsigned char command[COMMAND_SIZE]; // array for master to write to
unsigned char answer[ANSWER_SIZE]  =  {0}; // array to send answer to master
unsigned char hk_data[] = {0x06,0x67,0x06,0x66,255,255,255,255,255,255};
unsigned char CMD[COMMAND_SIZE];
unsigned char CR = 0x0D;

void Init_StateMachine(void);
void StateMachine(void);

#endif	/* XC_HEADER_TEMPLATE_H */