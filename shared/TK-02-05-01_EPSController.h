/* Ten-Koh TK-02-05-01 EPS COntroller PCB header file, which defines all the pins
 * of the two included PICs.
 * 
 * Intended for use with PIC16F877 and version of the PCB at least 1.1.6.
 */
#ifndef TK_02_05_01
#define	TK_02_05_01

/*
 * 
 */

#include <stdint.h> //To use uint_16, etc.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xc.h>
#include "spi_slave.h"
#include "i2c_slave.h"
#include "uart.h"
#include "StateMachine.h"
#include "OnBoardCommands.h"
#include "I2CAddresses.h"
#include "adc.h"
#include "spi_master.h"
#include "test_EPScontroller.h" //Only for testing, remove for final version
/*
********************************************************************************
* Modes definitions
********************************************************************************
*/

enum modes { CUSTOM = 0, MODE_1 = 1,  MODE_2 = 2, MODE_3 = 3};   //CUSTOM MODE REQUIRE extradata to set the pins

/*
********************************************************************************
* Variable definitions
********************************************************************************
*/

uint8_t mode = 0;
int16_t count1 = 78;
int16_t count2 = 78;
uint8_t SEC_FLAG = 0;
uint8_t H_SEC_FLAG = 0;
uint8_t multiplier = 1;
unsigned char SPI_FLAG = 0;
unsigned char SD_EN = 1;
unsigned char data_1[10] = {20,40,60,80};
unsigned char hk_string[20] = {0};
unsigned char status_reg[3] = {0}; //one byte for fmult and two status of outputs
unsigned char noExtraData[1] = {0}; //only to set when there is nothing else to send
uint16_t ADCdata[8]; //results of ADC conversion
adc_communication_information_t adc_trans_data; // Re-use for ADCs to save space.

/*
 *******************************************************************************
 *  Pin definitions.
 *******************************************************************************
 */

// SPI Master pins and initialisation registers for SPI_EPS (SPI is master).
#define SPI_MOSI_MASTER RB3 // OUT = MOSI - master out slave in. Data to slave
#define SPI_MISO_MASTER RB1 // IN = MISO - master in slave out. Data from slave
#define SPI_CK_MASTER   RB2 // OUT = SCK - SPI clock
#define SPI_MOSI_MASTER_INIT TRISB3
#define SPI_MISO_MASTER_INIT TRISB1
#define SPI_CK_MASTER_INIT TRISB2

// SPI slave pins for the SPI_BUS (EPS is slave).
#define SPI_CS_SLAVE   RB0 // IN = active-low CS?chip select. Only RB0 can be
                           // configured to falling edge interrupts.
#define SPI_MOSI_SLAVE RE0 // IN = MOSI?master out slave in. Data to slave
#define SPI_MISO_SLAVE RA5 // OUT = MISO?master in slave out. Data from slave
#define SPI_CK_SLAVE   RA2 // IN = SCK - SPI clock
#define SPI_CS_SLAVE_INIT TRISB0
#define SPI_MOSI_SLAVE_INIT TRISE0
#define SPI_MISO_SLAVE_INIT TRISA5
#define SPI_CK_SLAVE_INIT TRISA2

// PIC_M - PIC_B interface.
#define MCU_BCU_RESET RA4 // Used by the two PICs to reset each other.
#define UART_RX RC7 // Rx on PIC_M, Tx on PIC_B.
#define UART_TX RC6 // Vice-versa, Tx on PIC_M.
#define MCU_BCU_RESET_INIT TRISA4
#define UART_RX_INIT TRISC7
#define UART_TX_INIT TRISC6

// System-wide I2C, using the in-built PIC module.
#define I2C_SCL RC3
#define I2C_SDL RC4
#define I2C_SCL_INIT TRISC3
#define I2C_SDL_INIT TRISC4

// Misc. internal pins.

// CS for the EPS Controller SD card is actually defined in the
// SD sd_communication_information_t struct.
#define SPI_EPS_CS_SD RD2
#define SPI_EPS_CS_SD_INIT TRISD2
#define SPI_EPS_CS_OCPSTMUX RD5 // Status bits of all the power lines.
#define SPI_EPS_CS_OCPSTMUX_INIT TRISD5
#define SPI_EPS_CS_BATT_ADC RD4 // ADC in IF_SW that measures the battery temperatures, voltages and currents.
#define SPI_EPS_CS_BATT_ADC_INIT TRISD4
#define SPI_EPS_CS_REGBUS_ADC RB6 // REG_BUS ADC.
#define SPI_EPS_CS_REGBUS_ADC_INIT TRISB6
#define SPI_EPS_CS_REGPL_ADC RB7 // REG_PL ADC.
#define SPI_EPS_CS_REGPL_ADC_INIT TRISB7

// REG_BUS power line controls. Net names have numbers elsewhere but MPLAB doesn't like it.
// These lines are used by the S/C bus and are enabled by default with pull-ups.
#define FIVE_V_COM1_EN RB5
#define FIVE_V_COM2_EN RD7
#define FIVE_V_OBC_EN RD6
#define FIVE_V_COM1_EN_INIT TRISB5
#define FIVE_V_COM2_EN_INIT TRISD7
#define FIVE_V_OBC_EN_INIT TRISD6
// Payload but located on REG_BUS for convenience. Disabled by default.
#define FIVE_V_UCP_EN RD1
#define FIVE_V_UCP_EN_INIT TRISD1

// REG_PL power line controls. Net names have numbers elsewhere but MPLAB doesn't like it.
// These lines are disabled by default.
#define FIVE_V_PL_EN RD0
#define FIVE_V_DLP_EN RC2
#define TWELVE_V_CPD_EN RC1
#define TWELVE_V_ADS_EN RC0
#define FIVE_V_PL_EN_INIT TRISD0
#define FIVE_V_DLP_EN_INIT TRISC2
#define TWELVE_V_CPD_EN_INIT TRISC1
#define TWELVE_V_ADS_EN_INIT TRISC0

// Heater OCP enable and on commands.
#define HEAT_EN RE2
#define HEAT_ON_2 RE1
#define HEAT_ON_1 RA1
#define HEAT_EN_INIT TRISE2
#define HEAT_ON_2_INIT TRISE1
#define HEAT_ON_1_INIT TRISA1

// Kill switch and PIC_SENSE interface.
#define KILL_SET RD3
#define KILL_RESET RC5
#define PIC_SENSE_RESET RA3
#define KILL_SET_INIT TRISD3
#define KILL_RESET_INIT TRISC5
#define PIC_SENSE_RESET_INIT TRISA3

// Interrupts to and from the EPS controller.
#define GPIO_OBC_EPS RA0
#define GPIO_SAT_RESET RB4
#define GPIO_OBC_EPS_INIT TRISA0
#define GPIO_SAT_RESET_INIT TRISB4

#define ANALOGUE_PIN_SETUP 0x06 // Use this with ADCON1 to set RE<0:2>, RA0, RA1,
    // RA2, RA3 and RA5 to digital, as needed by this PIC.
#define UART_BAUD 9600 // Bits per second.
#define TIM_CYC	78 //Timer cycles to get to 998.4 ms 
#define H_TIM_CYC 39 //Timer cycles to get to 998.4 ms 




/*
 * Functions declarations
 */

void EPS_adc_read(void);
void GPIOInit(void);
void EpsSPImasterInit(void);
void Enable(unsigned char command);
void Disable(unsigned char command);
void Reset(unsigned char command);
void TIMER2Init(void);
void TIMERCount(uint8_t fmult);
void SetShortAnswer(unsigned char* fill, unsigned char size );
void SetAnswer(unsigned char* hk, unsigned char hk_size, unsigned char* stat, unsigned char stat_size);
void PrepareHK(uint16_t* data, unsigned char size);
void PrepareStatus(void);
void PrintAnswer(unsigned char* buffer, unsigned char hk_size, unsigned char stat_size);
void interruptInit(void);
void read_ADC(volatile unsigned char *port, unsigned char pin, uint16_t data[]);


#endif	/* XC_HEADER_TEMPLATE_H */

